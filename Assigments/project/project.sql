-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: myblog
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `commenter` varchar(255) DEFAULT NULL,
  `time` datetime NOT NULL,
  `postid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postid` (`postid`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`postid`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Coments','Testing','Anu','0000-00-00 00:00:00',6),(2,'nndnnd','c   jmxxxxxxxxxxxxxx','Anu','0000-00-00 00:00:00',22);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `existingusers`
--

DROP TABLE IF EXISTS `existingusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `existingusers` (
  `username` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `Uenable` int(10) DEFAULT '0',
  `Uapprove` int(10) DEFAULT '0',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `existingusers`
--

LOCK TABLES `existingusers` WRITE;
/*!40000 ALTER TABLE `existingusers` DISABLE KEYS */;
INSERT INTO `existingusers` VALUES ('aanil','Anu1','Vis','0',2147483647,'*4EAF9411A883B32F3579E5310A611CE05D0449FA',1,1),('anilviswa','Anil','Viswanadha','anil12@gmail.com',2147483647,'*56C7822B1A2F0EBD93CB775BB1538D3D2449DE3E',1,1),('anu123','anu','viswanadha','anu@gmail.com',290027388,'*CAB690696E278EAA3859FD781D42E2DE1D2BFC1C',1,1),('anuvis','Anuroopa','Viswanadha','anuroopa2107@gmail.com',2147483647,'*8B82D4DC1514282CAEF77F209C63E2FBFE30881C',1,1);
/*!40000 ALTER TABLE `existingusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `published` datetime NOT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `penable` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`),
  CONSTRAINT `FK_existingusers` FOREIGN KEY (`owner`) REFERENCES `existingusers` (`username`) ON DELETE CASCADE,
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `existingusers` (`username`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Test','First post','0000-00-00 00:00:00',NULL,0),(4,'Test2','Second post','0000-00-00 00:00:00',NULL,0),(5,',kkkkkkk','ccccccccccc','0000-00-00 00:00:00',NULL,0),(6,'Post 29','Hi !!!','0000-00-00 00:00:00',NULL,0),(7,'New','abc','0000-00-00 00:00:00',NULL,0),(8,'anu ','anil\r\n','0000-00-00 00:00:00',NULL,0),(9,'anil','anil','0000-00-00 00:00:00',NULL,0),(11,'nvkdn','skik','0000-00-00 00:00:00',NULL,0),(12,'New user','nfhhhf','0000-00-00 00:00:00',NULL,0),(13,'ammmd','dkkks','0000-00-00 00:00:00',NULL,0),(14,'ammm','djjjd','0000-00-00 00:00:00',NULL,0),(15,'mdmmd','hshhhs','0000-00-00 00:00:00',NULL,0),(19,'ndhhhd','jkskis','0000-00-00 00:00:00',NULL,0),(20,'nndnd','wjjkkw','0000-00-00 00:00:00',NULL,0),(21,'hey','hey','0000-00-00 00:00:00',NULL,0),(22,'nhh','shhhh','0000-00-00 00:00:00','aanil',1),(23,'the end','heloo.......','0000-00-00 00:00:00','aanil',1),(24,'neeewww','phheewww','0000-00-00 00:00:00','aanil',1),(25,'New','Test','0000-00-00 00:00:00','aanil',0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `UApprove` int(10) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('admin','*01A6717B58FF5C7EAFFF6CB7C96F7428EA65FE4C',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-02  9:55:50
